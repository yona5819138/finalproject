<?php

namespace app\models;
use app\models\Department;
use app\models\Idtype;
use app\models\Species;
use Yii;

/**
 * This is the model class for table "follows".
 *
 * @property int $id
 * @property string $ark
 * @property string $department
 * @property string $species
 * @property string $date
 * @property string $Description
 * @property string $Weight
 * @property string $temperature
 * @property string $breathing
 * @property string $pulse
 * @property string $link
 */
class Follows extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'follows';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date'], 'safe'],
            [['Description'], 'string'],
            [['ark', 'department', 'species', 'Weight', 'temperature', 'breathing', 'pulse', 'link'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ark' => 'ארק',
            'department' => 'מחלקה',
            'species' => 'זן',
            'date' => 'תאריך',
            'Description' => 'תיאור הבעיה',
            'Weight' => 'משקל',
            'temperature' => 'טמפרטורה',
            'breathing' => 'נשימה',
            'pulse' => 'דופק',
            'link' => 'קישור',
        ];
    }
    public function getdepartment1()  // VIEW בשביל למשוך נתונים מ
    {
        return $this->hasOne(Department::className(), ['id' => 'department']);
    }
    public function getspecies2()  // VIEW בשביל למשוך נתונים מ
    {
        return $this->hasOne(Species::className(), ['id' => 'species']);
    }
}
