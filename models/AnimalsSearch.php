<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Animals;

/**
 * AnimalsSearch represents the model behind the search form of `app\models\Animals`.
 */
class AnimalsSearch extends Animals
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['ark', 'department', 'species', 'gender', 'name', 'birth_date', 'arrival_date', 'location', 'id_type', 'id_number', 'source'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Animals::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'birth_date' => $this->birth_date,
            'arrival_date' => $this->arrival_date,
        ]);

        $query->andFilterWhere(['like', 'ark', $this->ark])
            ->andFilterWhere(['like', 'department', $this->department])
            ->andFilterWhere(['like', 'species', $this->species])
            ->andFilterWhere(['like', 'gender', $this->gender])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'location', $this->location])
            ->andFilterWhere(['like', 'id_type', $this->id_type])
            ->andFilterWhere(['like', 'id_number', $this->id_number])
            ->andFilterWhere(['like', 'source', $this->source]);

        return $dataProvider;
    }
}
