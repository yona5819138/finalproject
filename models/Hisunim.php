<?php

namespace app\models;

use Yii;
use app\models\Department;
use app\models\Idtype;
use app\models\Species;

/**
 * This is the model class for table "hisunim".
 *
 * @property int $id
 * @property string $ark
 * @property string $department
 * @property string $species
 * @property string $typeH
 * @property string $nameH
 * @property string $manufacturer
 * @property string $AliveOrDead
 * @property string $date
 * @property string $nextDate
 */
class Hisunim extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hisunim';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date', 'nextDate'], 'safe'],
            [['ark', 'department', 'species', 'typeH', 'nameH', 'manufacturer', 'AliveOrDead'], 'string', 'max' => 255],
            [['ark'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ark' => 'ארק',
            'department' => 'מחלקה',
            'species' => 'זן',
            'typeH' => 'סוג חיסון',
            'nameH' => 'שם החיסון',
            'manufacturer' => 'יצרן',
            'AliveOrDead' => 'חי/מומת',
            'date' => 'תאריך מתן',
            'nextDate' => 'תאריך הבא',
        ];
    }
    public function getdepartment1()  // VIEW בשביל למשוך נתונים מ
    {
        return $this->hasOne(Department::className(), ['id' => 'department']);
    }
    public function getspecies2()  // VIEW בשביל למשוך נתונים מ
    {
        return $this->hasOne(Species::className(), ['id' => 'species']);
    }
}
 