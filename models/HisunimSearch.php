<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Hisunim;

/**
 * HisunimSearch represents the model behind the search form of `app\models\Hisunim`.
 */
class HisunimSearch extends Hisunim
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['ark', 'department', 'species', 'typeH', 'nameH', 'manufacturer', 'AliveOrDead', 'date', 'nextDate'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Hisunim::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date' => $this->date,
            'nextDate' => $this->nextDate,
        ]);

        $query->andFilterWhere(['like', 'ark', $this->ark])
            ->andFilterWhere(['like', 'department', $this->department])
            ->andFilterWhere(['like', 'species', $this->species])
            ->andFilterWhere(['like', 'typeH', $this->typeH])
            ->andFilterWhere(['like', 'nameH', $this->nameH])
            ->andFilterWhere(['like', 'manufacturer', $this->manufacturer])
            ->andFilterWhere(['like', 'AliveOrDead', $this->AliveOrDead]);

        return $dataProvider;
    }
}
