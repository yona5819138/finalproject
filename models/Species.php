<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "species".
 *
 * @property int $id
 * @property string $hebrew_name
 * @property string $english_name
 * @property string $scientific_name
 */
class Species extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'species';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hebrew_name', 'english_name', 'scientific_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'hebrew_name' => 'Hebrew Name',
            'english_name' => 'English Name',
            'scientific_name' => 'Scientific Name',
        ];
    }
}
