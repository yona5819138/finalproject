<?php

namespace app\models;

use Yii;
use app\models\Department;
use app\models\Idtype;
use app\models\Species;


/**
 * This is the model class for table "tsoa".
 *
 * @property int $id
 * @property string $ark
 * @property string $department
 * @property string $species
 * @property string $negOrPos
 * @property string $finding
 * @property string $negiut
 * @property string $date
 * @property string $link
 */
class Tsoa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tsoa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ark', 'department', 'species', 'negOrPos', 'finding', 'negiut', 'date', 'link'], 'string', 'max' => 255],
            [['ark'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ark' => 'ארק',
            'department' => 'מחלקה',
            'species' => 'זן',
            'negOrPos' => 'חיובי/שלילי',
            'finding' => 'ממצא',
            'negiut' => 'רמת נגיעות',
            'date' => 'תאריך',
            'link' => 'קישור',
        ];
    }
    public function getdepartment1()  // VIEW בשביל למשוך נתונים מ
    {
        return $this->hasOne(Department::className(), ['id' => 'department']);
    }
    public function getspecies2()  // VIEW בשביל למשוך נתונים מ
    {
        return $this->hasOne(Species::className(), ['id' => 'species']);
    }
    public function getidtype3()  // VIEW בשביל למשוך נתונים מ
    {
        return $this->hasOne(Idtype::className(), ['id' => 'id_type']);
    }
}
