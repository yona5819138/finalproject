<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "medication".
 *
 * @property int $id
 * @property string $medicationName
 */
class Medication extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'medication';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['medicationName'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'medicationName' => 'Medication Name',
        ];
    }
}
