<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Virology;

/**
 * VirologySearch represents the model behind the search form of `app\models\Virology`.
 */
class VirologySearch extends Virology
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['ark', 'department', 'species', 'date', 'negOrPos', 'finding', 'type', 'link'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Virology::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'ark', $this->ark])
            ->andFilterWhere(['like', 'department', $this->department])
            ->andFilterWhere(['like', 'species', $this->species])
            ->andFilterWhere(['like', 'date', $this->date])
            ->andFilterWhere(['like', 'negOrPos', $this->negOrPos])
            ->andFilterWhere(['like', 'finding', $this->finding])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'link', $this->link]);

        return $dataProvider;
    }
}
