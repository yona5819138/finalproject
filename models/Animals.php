<?php

namespace app\models;

use Yii;
use app\models\Department;
use app\models\Idtype;
use app\models\Species;

/**
 * This is the model class for table "animals".
 *
 * @property int $id
 * @property string $ark
 * @property string $department
 * @property string $species
 * @property string $gender
 * @property string $name
 * @property string $birth_date
 * @property string $arrival_date
 * @property string $location
 * @property string $id_type
 * @property string $id_number
 * @property string $source
 */
class Animals extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'animals';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['birth_date', 'arrival_date'], 'safe'],
            [['ark', 'department', 'species', 'gender', 'name', 'location', 'id_type', 'id_number', 'source'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ark' => 'ארק',
            'department' => 'מחלקה',
            'species' => 'זנים',
            'gender' => 'מין',
            'name' => 'שם החיה',
            'birth_date' => 'תאריך לידה',
            'arrival_date' => 'תאריך הגעה לגן',
            'location' => 'מיקום',
            'id_type' => 'סוג זיהוי',
            'id_number' => 'מספר זיהוי',
            'source' => 'מקור',
        ];
    }
    public function getdepartment1()  // VIEW בשביל למשוך נתונים מ
    {
        return $this->hasOne(Department::className(), ['id' => 'department']);
    }
    public function getspecies2()  // VIEW בשביל למשוך נתונים מ
    {
        return $this->hasOne(Species::className(), ['id' => 'species']);
    }
    public function getidtype3()  // VIEW בשביל למשוך נתונים מ
    {
        return $this->hasOne(Idtype::className(), ['id' => 'id_type']);
    }
}
