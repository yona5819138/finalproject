<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Rentgen;

/**
 * RentgenSearch represents the model behind the search form of `app\models\Rentgen`.
 */
class RentgenSearch extends Rentgen
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['ark', 'department', 'species', 'date', 'comment', 'link'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Rentgen::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'ark', $this->ark])
            ->andFilterWhere(['like', 'department', $this->department])
            ->andFilterWhere(['like', 'species', $this->species])
            ->andFilterWhere(['like', 'date', $this->date])
            ->andFilterWhere(['like', 'comment', $this->comment])
            ->andFilterWhere(['like', 'link', $this->link]);

        return $dataProvider;
    }
}
