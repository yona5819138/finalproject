<?php

namespace app\models;

use Yii;
use app\models\Department;
use app\models\Idtype; 
use app\models\Species;

/**
 * This is the model class for table "ultrasound".
 *
 * @property int $id
 * @property string $ark
 * @property string $department
 * @property string $species
 * @property string $date
 * @property string $comment
 * @property string $link
 */
class Ultrasound extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ultrasound';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ark', 'department', 'species', 'date', 'comment', 'link'], 'string', 'max' => 255],
            [['ark'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ark' => 'ארק',
            'department' => 'מחלקה',
            'species' => 'זן',
            'date' => 'תאריך',
            'comment' => 'הערות',
            'link' => 'קישור',
        ];
    }
    public function getdepartment1()  // VIEW בשביל למשוך נתונים מ
    {
        return $this->hasOne(Department::className(), ['id' => 'department']);
    }
    public function getspecies2()  // VIEW בשביל למשוך נתונים מ
    {
        return $this->hasOne(Species::className(), ['id' => 'species']);
    }
    public function getidtype3()  // VIEW בשביל למשוך נתונים מ
    {
        return $this->hasOne(Idtype::className(), ['id' => 'id_type']);
    }
}
