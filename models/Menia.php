<?php

namespace app\models;

use Yii;
use app\models\Department;
use app\models\Idtype;
use app\models\Species;


/**
 * This is the model class for table "menia".
 *
 * @property int $id
 * @property string $ark
 * @property string $department
 * @property string $species
 * @property string $typeM
 * @property string $nameM
 * @property string $manufacturer
 * @property string $date
 * @property string $nextDate
 */
class Menia extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'menia';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date', 'nextDate'], 'safe'],
            [['ark', 'department', 'species', 'typeM', 'nameM', 'manufacturer'], 'string', 'max' => 255],
            [['ark'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ark' => 'ארק',
            'department' => 'מחלקה',
            'species' => 'זן',
            'typeM' => 'סוג',
            'nameM' => 'שם ',
            'manufacturer' => 'יצרן',
            'date' => 'תאריך',
            'nextDate' => 'תאריך הבא',
        ];
    }
    public function getdepartment1()  // VIEW בשביל למשוך נתונים מ
    {
        return $this->hasOne(Department::className(), ['id' => 'department']);
    }
    public function getspecies2()  // VIEW בשביל למשוך נתונים מ
    {
        return $this->hasOne(Species::className(), ['id' => 'species']);
    }
}
