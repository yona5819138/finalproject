<?php

namespace app\models;

use Yii;
use app\models\Medication;
use app\models\Department;
use app\models\Idtype;
use app\models\Species;
use app\models\Frequency;
use app\models\Route;


/**
 * This is the model class for table "provideDrug".
 *
 * @property int $id
 * @property string $ark
 * @property string $department
 * @property string $species
 * @property string $medicationName
 * @property string $ricuz
 * @property string $recommended_dosage
 * @property string $dose
 * @property string $volume
 * @property string $frequency
 * @property string $route
 * @property string $start_date
 * @property string $finish_date
 * @property string $comment
 */
class ProvideDrug extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'provideDrug';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['start_date', 'finish_date'], 'safe'],
            [['ark', 'department', 'species', 'medicationName', 'ricuz', 'recommended_dosage', 'dose', 'volume', 'frequency', 'route', 'comment'], 'string', 'max' => 255],
            [['ark'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ark' => 'ארק',
            'department' => 'מחלקה',
            'species' => 'זן',
            'medicationName' => 'שם התרופה',
            'ricuz' => 'ריכוז',
            'recommended_dosage' => 'recommended dosage (mg/kg) ',
            'dose' => 'Dose (mg)',
            'volume' => 'Volume (ml)',
            'frequency' => 'Frequency',
            'route' => 'Route',
            'start_date' => 'תאריך קבלה',
            'finish_date' => 'תאריך סיום',
            'comment' => 'הערות', 
        ];
    }
    public function getdepartment1()  // VIEW בשביל למשוך נתונים מ
    {
        return $this->hasOne(Department::className(), ['id' => 'department']);
    }
    public function getspecies2()  // VIEW בשביל למשוך נתונים מ
    {
        return $this->hasOne(Species::className(), ['id' => 'species']);
    }
    public function getfrequency1()  // VIEW בשביל למשוך נתונים מ
    {
        return $this->hasOne(Frequency::className(), ['id' => 'frequency']);
    }
    public function getRoute2()  // VIEW בשביל למשוך נתונים מ
    {
        return $this->hasOne(Route::className(), ['id' => 'route']);
    }
    public function getMedication1()  // VIEW בשביל למשוך נתונים מ
    {
        return $this->hasOne(Medication::className(), ['id' => 'medicationName']);
    }
}
