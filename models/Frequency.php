<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "frequency".
 *
 * @property int $id
 * @property string $frequency
 */
class Frequency extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'frequency';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['frequency'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'frequency' => 'תדירות',
        ];
    }
}
