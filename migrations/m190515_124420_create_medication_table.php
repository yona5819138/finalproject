<?php

use yii\db\Migration;

/**
 * Handles the creation of table `medication`.
 */
class m190515_124420_create_medication_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('medication', [
            'id' => $this->primaryKey(),
            'medicationName' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('medication');
    }
}
