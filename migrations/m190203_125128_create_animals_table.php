<?php

use yii\db\Migration;

/**
 * Handles the creation of table `animals`.
 */
class m190203_125128_create_animals_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    
    {
        $this->createTable('animals', [
            'id' => $this->primaryKey(),
            'ark' => $this->string()->unique(),
            'department' => $this->string(),//טבלה
            'species' => $this->string(),//טבלה
            'gender' => $this->string(),
            'name' => $this->string(),
            'birth_date' => $this->date(),
            'arrival_date' => $this->date(),
            'location' => $this->string(),
            'id_type' => $this->string(),//טבלה
            'id_number' => $this->string(),
            'source' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('animals');
    }
}
