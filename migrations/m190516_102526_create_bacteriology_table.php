<?php

use yii\db\Migration;

/**
 * Handles the creation of table `bacteriology`.
 */
class m190516_102526_create_bacteriology_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('bacteriology', [
            'id' => $this->primaryKey(),
            'ark' => $this->string()->unique(),
            'department' => $this->string(),//טבלה
            'species' => $this->string(),//טבלה
            'date'=> $this->string(),
            'negOrPos' => $this->string(),
            'Bacteria' => $this->string(),
            'sensitivity'=> $this->string(),
            'link' => $this->string()        
            ]);
    }
    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('bacteriology');
    }
}
