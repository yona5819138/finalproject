<?php

use yii\db\Migration;

/**
 * Handles the creation of table `virology`.
 */
class m190516_125406_create_virology_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('virology', [
            'id' => $this->primaryKey(),
            'ark' => $this->string()->unique(),
            'department' => $this->string(),//טבלה
            'species' => $this->string(),//טבלה
            'date'=> $this->string(),
            'negOrPos' => $this->string(),
            'finding' => $this->string(),
            'type' => $this->string(),
            'link' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('virology');
    }
}
