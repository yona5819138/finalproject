<?php

use yii\db\Migration;

/**
 * Handles the creation of table `Endoscopy`.
 */
class m190516_075339_create_Endoscopy_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('Endoscopy', [
            'id' => $this->primaryKey(),
            'ark' => $this->string()->unique(),
            'department' => $this->string(),//טבלה
            'species' => $this->string(),//טבלה
            'date' => $this->string(),
            'comment' => $this->string(),
            'link' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('Endoscopy');
    }
}
