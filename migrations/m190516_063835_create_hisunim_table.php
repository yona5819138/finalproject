<?php

use yii\db\Migration;

/**
 * Handles the creation of table `hisunim`.
 */
class m190516_063835_create_hisunim_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('hisunim', [
            'id' => $this->primaryKey(),
            'ark' => $this->string()->unique(),
            'department' => $this->string(),//טבלה
            'species' => $this->string(),//טבלה
            'typeH' => $this->string(),
            'nameH' => $this->string(),
            'manufacturer'=> $this->string(),
            'AliveOrDead' => $this->string(),
            'date' => $this->date(),
            'nextDate' => $this->date(),
            
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('hisunim');
    }
}
