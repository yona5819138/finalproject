<?php

use yii\db\Migration;

/**
 * Handles the creation of table `menia`.
 */
class m190516_073359_create_menia_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('menia', [
            'id' => $this->primaryKey(),
            'ark' => $this->string()->unique(),
            'department' => $this->string(),//טבלה
            'species' => $this->string(),//טבלה
            'typeM' => $this->string(),
            'nameM' => $this->string(),
            'manufacturer'=> $this->string(),
            'date' => $this->date(),
            'nextDate' => $this->date(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('menia');
    }
}
