<?php

use yii\db\Migration;

/**
 * Handles the creation of table `species`.
 */
class m190203_134002_create_species_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('species', [
            'id' => $this->primaryKey(),
            'hebrew_name' => $this->string(),
            'english_name' => $this->string(),
            'scientific_name' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('species');
    }
}
