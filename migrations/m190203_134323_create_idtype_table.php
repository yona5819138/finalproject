<?php

use yii\db\Migration;

/**
 * Handles the creation of table `idtype`.
 */
class m190203_134323_create_idtype_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('idtype', [
            'id' => $this->primaryKey(),
            'type' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('idtype');
    }
}
