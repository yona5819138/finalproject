<?php

use yii\db\Migration;

/**
 * Handles the creation of table `provideDrug`.
 */
class m190515_133521_create_provideDrug_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('provideDrug', [
            'id' => $this->primaryKey(),
            'ark' => $this->string()->unique(),
            'department' => $this->string(),//טבלה
            'species' => $this->string(),//טבלה
            'medicationName' => $this->string(),//טבלה
            'ricuz' => $this->string(),
            'recommended_dosage'=> $this->string(),
            'dose' => $this->string(),
            'volume' => $this->string(),
            'frequency' => $this->string(),//טבלה
            'route'=> $this->string(),//טבלה
            'start_date' => $this->date(),
            'finish_date' => $this->date(),
            'comment' => $this->string(),


        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('provideDrug');
    }
}
