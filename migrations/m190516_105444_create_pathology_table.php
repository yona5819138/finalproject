<?php

use yii\db\Migration;

/**
 * Handles the creation of table `pathology`.
 */
class m190516_105444_create_pathology_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('pathology', [
            'id' => $this->primaryKey(),
            'ark' => $this->string()->unique(),
            'department' => $this->string(),//טבלה
            'species' => $this->string(),//טבלה
            'date'=> $this->string(),
            'finding' => $this->string(),
            'link' => $this->string(),
        ]);
    }
    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('pathology');
    }
}
