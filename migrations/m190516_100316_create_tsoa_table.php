<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tsoa`.
 */
class m190516_100316_create_tsoa_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('tsoa', [
            'id' => $this->primaryKey(),
            'ark' => $this->string()->unique(),
            'department' => $this->string(),//טבלה
            'species' => $this->string(),//טבלה
            'negOrPos' => $this->string(),
            'finding' => $this->string(),
            'negiut'=> $this->string(),
            'date'=> $this->string(),
            'link' => $this->string(),
        ]);
    }
    // ארק , תאריך ,שלילי\חיובי, ממצא, רמת נגיעות ,קישור לתמונה.
    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('tsoa');
    }
}
