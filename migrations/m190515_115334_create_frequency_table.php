<?php

use yii\db\Migration;

/**
 * Handles the creation of table `frequency`.
 */
class m190515_115334_create_frequency_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('frequency', [
            'id' => $this->primaryKey(),
            'frequency' => $this->string(),

        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('frequency');
    }
}
