<?php

use yii\db\Migration;

/**
 * Handles the creation of table `ultrasound`.
 */
class m190516_075305_create_ultrasound_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('ultrasound', [
            'id' => $this->primaryKey(),
            'ark' => $this->string()->unique(),
            'department' => $this->string(),//טבלה
            'species' => $this->string(),//טבלה
            'date' => $this->string(),
            'comment' => $this->string(),
            'link' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('ultrasound');
    }
}
