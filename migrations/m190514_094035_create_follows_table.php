<?php

use yii\db\Migration;

/**
 * Handles the creation of table `follows`.
 */
class m190514_094035_create_follows_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('follows', [
            'id' => $this->primaryKey(),
            'ark' => $this->string(),
            'department' => $this->string(),//טבלה
            'species' => $this->string(),//טבלה
            'date' => $this->date(),
            'Description' => $this->text(),
            'Weight' => $this->string(),
            'temperature' => $this->string(),
            'breathing' => $this->string(),
            'pulse' => $this->string(),
            'link' => $this->string(),
           
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('follows');
    }
}
