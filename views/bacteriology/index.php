<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BacteriologySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Bacteriologies';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bacteriology-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Bacteriology', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'ark',
            [
                'label' => 'מחלקה',
                'attribute'=> 'department',
                'value' =>'department1.name'
            ],
           
            [
                'label' =>  'זן',
                'attribute'=> 'species',
                'value' =>'species2.hebrew_name'
            ],
            'date',
            'negOrPos',
            'Bacteria',
            'sensitivity',
            'link',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
