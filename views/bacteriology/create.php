<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Bacteriology */

$this->title = 'Create Bacteriology';
$this->params['breadcrumbs'][] = ['label' => 'Bacteriologies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bacteriology-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
