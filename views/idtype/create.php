<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Idtype */

$this->title = 'Create Idtype';
$this->params['breadcrumbs'][] = ['label' => 'Idtypes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="idtype-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
