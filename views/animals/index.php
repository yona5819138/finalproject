<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AnimalsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'חיות';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="animals-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('צור חיה חדשה', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'ark',
         
            [
                'label' => 'מחלקה',
                'attribute'=> 'department',
                'value' =>'department1.name'
            ],
           
            [
                'label' =>  'זן',
                'attribute'=> 'species',
                'value' =>'species2.hebrew_name'
            ],
           
            'gender',
            'name',
            'location',
            'birth_date',
            'arrival_date',
           'birth_date',
            'arrival_date',
            [
                'label' =>   'סוג זיהוי',
                'attribute'=> 'id_type',
                'value' => 'idtype3.type'
            ],
            'id_number',
            'source',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
