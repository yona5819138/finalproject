<?php
use app\models\Department;
use app\models\Idtype;
use app\models\Species;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

//use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Animals */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="animals-form" style="margin-right:800px; margin-left:0px;">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ark')->textInput(['maxlength' => true]) ?>
     
    
    <div class="row"> 
        <div class="col-md-10">
        
        <?= $form->field($model, 'department')->dropDownList( 
            ArrayHelper::map(Department::find()->asArray()->all(), 'id', 'name')  ) ?>
        </div> 
        <div class="col-md-2">
            <span class="input-group-btn">
            <a href="../department/create" id="stat" class="btn btn-primary" style="margin-top:23px; margin-left:0px; border-radius:10px;">Add Category</a>
            </span>
        </div>
    </div>

    <div class="row">
        <div class="col-md-10">
        
        <?= $form->field($model, 'species')->dropDownList(
       ArrayHelper::map(Species::find()->asArray()->all(), 'id', 'hebrew_name')) ?>
        </div> 
        <div class="col-md-2">
            <span class="input-group-btn">
            <a href="../species/create" id="stat" class="btn btn-primary" style="margin-top:23px; margin-left:0px; border-radius:10px;">Add Category</a>
            </span>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10">
        
        <?= $form->field($model, 'id_type')->dropDownList(
       ArrayHelper::map(Idtype::find()->asArray()->all(), 'id', 'type')) ?>
        </div> 
        <div class="col-md-2">
            <span class="input-group-btn">
            <a href="../idtype/create" id="stat" class="btn btn-primary" style="margin-top:23px; margin-left:0px; border-radius:10px;">Add Category</a>
            </span>
        </div>
    </div>
    
    <?= $form->field($model, 'id_number')->textInput(['maxlength' => true]) ?>
     
    
    
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'birth_date')->input('date') ?>

    <?= $form->field($model, 'arrival_date')->input('date') ?>

    <?= $form->field($model, 'location')->textInput(['maxlength' => true]) ?>

   

    <?= $form->field($model, 'source')->textInput(['maxlength' => true]) ?>

       <h5 style = "font-weight:bold">Gender</h5>
    <div style = "margin-left:20px;"> 
     <?= $form->field($model, 'gender')->radio(['label' => 'male', 'value' => 'male', 'uncheck' => null]) ?>
     <?= $form->field($model, 'gender')->radio(['label' => 'female', 'value' => 'female', 'uncheck' => null]) ?>
     <?= $form->field($model, 'gender')->radio(['label' => 'unknown', 'value' => 'unknown', 'uncheck' => null]) ?>
     <?= $form->field($model, 'gender')->radio(['label' => 'group', 'value' => 'group', 'uncheck' => null]) ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
