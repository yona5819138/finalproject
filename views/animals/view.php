<?php
use app\models\Department;
use app\models\Idtype;
use app\models\Species;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Animals */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Animals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="animals-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'ark',

            [
                'label' => 'מחלקה',
                'value' => $model->department1->name
            ],
           
            [
                'label' =>  'זן',
                'value' => $model->species2->hebrew_name
            ],
            'gender',
            'name',
            'birth_date', 
            'arrival_date',
            'location',
          
            [
                'label' =>   'סוג זיהוי',
                'value' => $model->idtype3->type
            ],
            'id_number',
            'source',
        ],
    ]) ?>

</div>
