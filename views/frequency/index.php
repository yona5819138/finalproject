<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FrequencySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Frequencies';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="frequency-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Frequency', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'id',
            'frequency',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
