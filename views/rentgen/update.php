<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Rentgen */

$this->title = 'Update Rentgen: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Rentgens', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="rentgen-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
