<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Rentgen */

$this->title = 'Create Rentgen';
$this->params['breadcrumbs'][] = ['label' => 'Rentgens', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rentgen-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
