<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Medication;
use app\models\Department;
use app\models\Route;
use app\models\Species;
use app\models\Frequency;

/* @var $this yii\web\View */
/* @var $model app\models\ProvideDrug */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="provide-drug-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ark')->textInput(['maxlength' => true]) ?>

 
<?= $form->field($model, 'department')->dropDownList(
        ArrayHelper::map(Department::find()->asArray()->all(), 'id', 'name')  ) ?>

<?= $form->field($model, 'species')->dropDownList(
       ArrayHelper::map(Species::find()->asArray()->all(), 'id', 'hebrew_name')) ?>

<?= $form->field($model, 'medicationName')->dropDownList(
    ArrayHelper::map(Medication::find()->asArray()->all(), 'id', 'medicationName')) ?>


<?= $form->field($model, 'ricuz')->textInput(['maxlength' => true]) ?>


<?= $form->field($model, 'recommended_dosage')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'dose')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'volume')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'frequency')->dropDownList(
        ArrayHelper::map(Frequency::find()->asArray()->all(), 'id', 'frequency')) ?>

<?= $form->field($model, 'route')->dropDownList(
    ArrayHelper::map(Route::find()->asArray()->all(), 'id', 'route')) ?>


    <?= $form->field($model, 'start_date')->input('date') ?>

    <?= $form->field($model, 'finish_date')->input('date') ?>

    <?= $form->field($model, 'comment')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
