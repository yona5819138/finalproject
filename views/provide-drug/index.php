<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProvideDrugSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ניפוק תרופה';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="provide-drug-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Provide Drug', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'ark',
            [
                'label' => 'מחלקה',
                'attribute'=> 'department',
                'value' =>'department1.name'
            ],
           
            [
                'label' =>  'זן',
                'attribute'=> 'species',
                'value' =>'species2.hebrew_name'
            ],
            [
                'label' => 'medicationName',
                'attribute'=> 'medicationName',
                'value' =>'medication1.medicationName'
            ],
           
            [
                'label' =>  'frequency',
                'attribute'=> 'frequency',
                'value' =>'frequency1.frequency'
            ],
            [
                'label' =>  'route',
                'attribute'=> 'route',
                'value' =>'route2.route'
            ],
            'ricuz',
            'recommended_dosage',
            'dose',
            'volume',
            'start_date',
            'finish_date',
            'comment',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
