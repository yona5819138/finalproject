<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProvideDrug */

$this->title = 'Create Provide Drug';
$this->params['breadcrumbs'][] = ['label' => 'Provide Drugs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="provide-drug-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
