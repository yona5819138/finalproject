<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Medication;
use app\models\Department;
use app\models\Idtype;
use app\models\Species;
use app\models\Frequency;

/* @var $this yii\web\View */
/* @var $model app\models\ProvideDrug */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Provide Drugs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="provide-drug-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'ark',
            [
                'label' => 'מחלקה',
                'value' => $model->department1->name
            ],
           
            [
                'label' =>  'זן',
                'value' => $model->species2->hebrew_name
            ],
            [
                'label' =>  'medicationName',
                'value' => $model->medication1->medicationName
            ],
            'medicationName',
            'ricuz',
            'recommended_dosage',
            'dose',
            'volume',
            [
                'label' => 'frequency',
                'value' => $model->frequency1->frequency
            ],
           
            [
                'label' =>  'route',
                'value' => $model->route2->route
            ],
            
            'start_date',
            'finish_date',
            'comment',
        ],
    ]) ?>

</div>
