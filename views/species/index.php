<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SpeciesrSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Species';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="species-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Species', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'hebrew_name',
            'english_name',
            'scientific_name',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
