<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Species */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="species-form"  style="margin-right:800px; margin-left:0px;">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'hebrew_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'english_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'scientific_name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
