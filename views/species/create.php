<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Species */

$this->title = 'Create Species';
$this->params['breadcrumbs'][] = ['label' => 'Species', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="species-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
