<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Pathology */

$this->title = 'Update Pathology: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Pathologies', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pathology-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
