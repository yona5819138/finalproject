<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Pathology */

$this->title = 'Create Pathology';
$this->params['breadcrumbs'][] = ['label' => 'Pathologies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pathology-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
