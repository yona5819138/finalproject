<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PathologyrSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'פתולוגיה';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pathology-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('צור בדיקה חדשה', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'ark',
            [
                'label' => 'מחלקה',
                'attribute'=> 'department',
                'value' =>'department1.name'
            ],
           
            [
                'label' =>  'זן',
                'attribute'=> 'species',
                'value' =>'species2.hebrew_name'
            ],
            'date',
            'finding',
            'link',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
