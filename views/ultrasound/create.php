<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Ultrasound */

$this->title = 'Create Ultrasound';
$this->params['breadcrumbs'][] = ['label' => 'Ultrasounds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ultrasound-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
