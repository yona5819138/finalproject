<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Department;
use app\models\Idtype;
use app\models\Species;

/* @var $this yii\web\View */
/* @var $model app\models\Hisunim */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="hisunim-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ark')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'department')->dropDownList(
       ArrayHelper::map(Department::find()->asArray()->all(), 'id', 'name')  ) ?>

     <?= $form->field($model, 'species')->dropDownList(
       ArrayHelper::map(Species::find()->asArray()->all(), 'id', 'hebrew_name')) ?>
       
 
    <?= $form->field($model, 'typeH')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nameH')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'manufacturer')->textInput(['maxlength' => true]) ?>
<div>
 <h5 style = "font-weight:bold">חי או מומת</h5>
    <div style = "margin-left:20px;"> 
     <?= $form->field($model, 'AliveOrDead')->radio(['label' => 'חי', 'value' => 'חי', 'uncheck' => null]) ?>
     <?= $form->field($model, 'AliveOrDead')->radio(['label' => 'מומת', 'value' => 'מומת', 'uncheck' => null]) ?>

   </div>
    <?= $form->field($model, 'date')->input('date') ?>

    <?= $form->field($model, 'nextDate')->input('date') ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
