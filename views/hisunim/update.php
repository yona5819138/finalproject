<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Hisunim */

$this->title = 'Update Hisunim: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Hisunims', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="hisunim-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
