<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TsoaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tsoa-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'ark') ?>

    <?= $form->field($model, 'department') ?>

    <?= $form->field($model, 'species') ?>

    <?= $form->field($model, 'negOrPos') ?>

    <?php // echo $form->field($model, 'finding') ?>

    <?php // echo $form->field($model, 'negiut') ?>

    <?php // echo $form->field($model, 'date') ?>

    <?php // echo $form->field($model, 'link') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
