<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Tsoa */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Tsoas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="tsoa-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

      <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
           // 'id',
            'ark',
            [
                'label' => 'מחלקה',
                'value' => $model->department1->name
            ],
           
            [
                'label' =>  'זן',
                'value' => $model->species2->hebrew_name
            ],
            'negOrPos',
            'finding',
            'negiut',
            'date',
            'link',
        ],
    ]) ?>
</div>
