<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper; 
use app\models\Department;
use app\models\Idtype;
use app\models\Species;

/* @var $this yii\web\View */
/* @var $model app\models\Tsoa */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tsoa-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ark')->textInput(['maxlength' => true]) ?>

  <?= $form->field($model, 'department')->dropDownList(
       ArrayHelper::map(Department::find()->asArray()->all(), 'id', 'name'),   ['prompt'=>'בחר את המחלקה של החיה']  ) ?>

     <?= $form->field($model, 'species')->dropDownList(
        
       ArrayHelper::map(Species::find()->asArray()->all(), 'id', 'hebrew_name') ,   ['prompt'=>'בחר את הזן של החיה']) ?>

<div>
 <h5 style = "font-weight:bold">חיובי/שלילי</h5>
    <div style = "margin-left:20px;"> 
     <?= $form->field($model, 'negOrPos')->radio(['label' => 'חיובי', 'value' => 'חיובי', 'uncheck' => null]) ?>
     <?= $form->field($model, 'negOrPos')->radio(['label' => 'שלילי', 'value' => 'שלילי', 'uncheck' => null]) ?>

   </div>


    <?= $form->field($model, 'finding')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'negiut')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'date')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
