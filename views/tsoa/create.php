<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Tsoa */

$this->title = 'Create Tsoa';
$this->params['breadcrumbs'][] = ['label' => 'Tsoas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tsoa-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
