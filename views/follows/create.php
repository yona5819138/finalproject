<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Follows */

$this->title = 'Create Follows';
$this->params['breadcrumbs'][] = ['label' => 'Follows', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="follows-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
