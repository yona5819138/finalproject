<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FollowsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Follows';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="follows-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Follows', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'ark',
            [
                'label' => 'מחלקה',
                'value' =>'department1.name'
            ],
           
            [
                'label' =>  'זן',
                'value' =>'species2.hebrew_name'
            ],
            'date',
            'Description:ntext',
            'Weight',
            'temperature',
            'breathing',
            'pulse',
            'link',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
