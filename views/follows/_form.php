<?php
use app\models\Department;
use app\models\Idtype;
use app\models\Species;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Follows */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="follows-form">

    <?php $form = ActiveForm::begin(); ?> 

    <?= $form->field($model, 'ark')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'department')->dropDownList(
            ArrayHelper::map(Department::find()->asArray()->all(), 'id', 'name')  ) ?>

      <?= $form->field($model, 'species')->dropDownList(
       ArrayHelper::map(Species::find()->asArray()->all(), 'id', 'hebrew_name')) ?>

    <?= $form->field($model, 'date')->input('date') ?>

    <?= $form->field($model, 'Description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'Weight')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'temperature')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'breathing')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pulse')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
