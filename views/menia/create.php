<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Menia */

$this->title = 'Create Menia';
$this->params['breadcrumbs'][] = ['label' => 'Menias', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menia-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
