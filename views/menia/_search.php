<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MeniaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="menia-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'ark') ?>

    <?= $form->field($model, 'department') ?>

    <?= $form->field($model, 'species') ?>

    <?= $form->field($model, 'typeM') ?>

    <?php // echo $form->field($model, 'nameM') ?>

    <?php // echo $form->field($model, 'manufacturer') ?>

    <?php // echo $form->field($model, 'date') ?>

    <?php // echo $form->field($model, 'nextDate') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
