<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Endoscopy */

$this->title = 'Update Endoscopy: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Endoscopies', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="endoscopy-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
