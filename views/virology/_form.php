<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper; 
use app\models\Department;
use app\models\Idtype;
use app\models\Species;
/* @var $this yii\web\View */ 
/* @var $model app\models\Virology */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="virology-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ark')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'department')->dropDownList(
       ArrayHelper::map(Department::find()->asArray()->all(), 'id', 'name')  ) ?>

     <?= $form->field($model, 'species')->dropDownList(
       ArrayHelper::map(Species::find()->asArray()->all(), 'id', 'hebrew_name')) ?>

    <?= $form->field($model, 'date')->input('date') ?>

<div>
 <h5 style = "font-weight:bold">חיובי/שלילי</h5>
    <div style = "margin-left:20px;"> 
     <?= $form->field($model, 'negOrPos')->radio(['label' => 'חיובי', 'value' => 'חיובי', 'uncheck' => null]) ?>
     <?= $form->field($model, 'negOrPos')->radio(['label' => 'שלילי', 'value' => 'שלילי', 'uncheck' => null]) ?>

   </div>

      <?php
    echo $form->field($model, 'type')->dropDownList(
                ['prompt'=>'בחר את סוג הבדיקה','HI' => 'HI', 'PCR' => 'PCR', 'ELISA' => 'ELISA']); ?>

    <?= $form->field($model, 'finding')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
