<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Virology */

$this->title = 'Create Virology';
$this->params['breadcrumbs'][] = ['label' => 'Virologies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="virology-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
